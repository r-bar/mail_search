#!/bin/bash

#function finish {
#  jobs -p | xargs kill
#}
echo Waiting for X to start...
sleep 2
Xvfb $DISPLAY -screen 0 1024x768x24 &

eval "`dbus-launch`"
export DBUS_SESSION_BUS_ADDRESS
export DBUS_SESSION_BUS_PID
export DBUS_SESSION_BUS_WINDOWID

eval "`gnome-keyring-daemon`"
export GNOME_KEYRING_CONTROL
export GNOME_SSH_AUTH_SOCK

#x11vnc -forever -create Desktop-Bridge
ID=`dd if=/dev/urandom count=10 bs=1 2> /dev/null | base32`
mkdir -p /run/protonbridge/$ID
echo Run ID: $ID
PIPE_IN="/run/protonbridge/$ID/stdin"
PIPE_OUT="/run/protonbridge/$ID/stdout"
mkfifo $PIPE_IN $PIPE_OUT
Desktop-Bridge -c < $PIPE_IN > $PIPE_OUT &
#nc 0.0.0.0 
nc -l 0.0.0.0 8080 < $PIPE_OUT > $PIPE_IN
