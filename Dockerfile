FROM python:3.7.1-alpine
RUN mkdir /wheels
RUN apk update \
  && apk add --no-cache \
    autoconf \
    automake \
    g++ \
    gcc \
    libc6-compat \
    libtool \
    linux-headers \
    make \
    musl-dev
COPY requirements.txt .
RUN grep '^\(uvloop\|httptools\|regex\|ujson\)' requirements.txt > wheel-requirements.txt \
  && xargs pip wheel -w wheels -r wheel-requirements.txt
RUN ls

FROM python:3.7.1-alpine
RUN mkdir /app
WORKDIR /app
VOLUME /app/static
RUN apk update \
  && apk add --no-cache tini \
  && pip install -U pip pip-tools setuptools
COPY requirements.txt .
COPY --from=0 /wheels /tmp/wheels
RUN pip install /tmp/wheels/* && pip install -r requirements.txt
COPY static .
COPY web.py .
ENTRYPOINT ["tini", "--"]
CMD ["./web.py"]
