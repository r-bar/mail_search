#!/usr/bin/env python3

from argparse import ArgumentParser

from sanic import Sanic, response

app = Sanic()


def cli():
    p = ArgumentParser(description='An email search tool')
    p.add_argument('-p', '--port', default=8000, type=int)
    p.add_argument('-H', '--host', default='0.0.0.0')
    p.add_argument('-e', '--elasticsearch-url',
                   default='http://localhost:9200')
    return p


@app.route("/")
async def index(req):
    return response.json({'hello': 'world'})


def main():
    config = cli().parse_args()
    app.run(host=config.host, port=config.port)


if __name__ == '__main__':
    main()
